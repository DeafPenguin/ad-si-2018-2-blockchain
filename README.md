# BLOCKCHAIN
- Victor Murilo Balbino Machado (victormurilo@gmail.com)
- Universidade Federal de Goiás (UFG)
- Instituto de Informática (INF)
- Aplicações Distribuídas
- Goiânia - Goiás - Brasil

### Resumo

Este projeto tem como finalidade a criação de um tutorial para iniciantes em blockchain.

### Requisitos

Para este projeto é necessário ter:

- Java;
- JDK;
- IDE de Java (eclipse, etc);
- GSON (biblioteca da google que torna objetos em formato json);

### Introdução e justificativa


Blockchain é, como o próprio nome diz, uma cadeia de blocos. Cada bloco pertencente a esta cadeia possui informações que são utilizadas para as mais diversas finalidades. A tecnologia tem como principal objetivo autenticar estas informações inseridas de forma que não podem ser falsificadas ou adulteradas.
Objetivos
	
Introduzir aos desconhecedores do assunto sobre o tema e aprimorar daqueles que já conhecem, dissecando a estrutura de forma que possamos correlacionar a teoria com a prática. O estudo também aborda os pontos positivos (vantagens) e negativos (desvantagens) do blockchain de forma a impulsionar o seu uso em outras aplicações por meio da disseminação de informação.
Metodologia

- Identificação da literatura especializada sobre o assunto;
- Ler documentações existentes a fim de compreender a estrutura;

### Funcionamento

A figura abaixo demonstra de forma teórica um blockchain. Pode ser observado que cada um destes blocos possui informações (data) e que estes blocos são interligados uns aos outros. O tipo de dado armazenado depende do tipo de blockchain programado. O primeiro bloco de todo blockchain é chamado de Genesis Block (Bloco de Origem), o segundo bloco criado é ligado ao primeiro e assim por diante.

![Alt Text](https://www.guru99.com/images/1/053018_0719_BlockchainT2.png)

Fonte: https://www.guru99.com/images/1/053018_0719_BlockchainT2.png
 
Cada bloco possui um hash próprio e único, fazendo o papel de uma chave identificadora que identifica o próprio bloco e seu conteúdo. Sempre que houver uma modificação em determinado bloco previamente criado, o hash também vai ser modificado. Além disso, todo bloco possui armazenado o hash do bloco anterior, dessa forma criando a ligação entre eles.

### Proteção

Uma das formas de garantir a autenticidade do bloco é através do hash de direcionamento, no qual identifica o bloco anterior por meio deste. Caso haja alguma adulteração, o hash é modificado e torna inválido a ligação deste bloco e os posteriores.
Há a possibilidade de um violador conseguir modificar todos os hashes de uma cadeia de blocos, afim de tornar legítimo, através de tecnologia que executa em alta velocidade. Através deste método de Proof-of-work, é criado um atraso na criação de novos blocos afim de que o trabalho de modificar todos os blocos seja imensurável.
Outra forma, que é a mais notável, é o uso de rede distribuída. Ao invés do blockchain ser gerenciado por um único servidor, é utilizado uma rede P2P distribuída e que todos tem livre acesso. Quando uma máquina ingressa nesta rede, recebe uma cópia completa do blockchain e cada computador se torna um nó desta rede. Quando ocorre a inserção de um bloco novo na rede, todos os nós recebem este bloco e verificam se não foi adulterado e então, após a verificação e aprovação da maioria dos nós, este bloco é finalmente adicionado ao blockchain.
Usos

Atualmente, quando se ouve falar de blockchain automaticamente vem a tona o bitcoin e vice-versa. Porém o uso do blockchain não é restrito ao mundo financeiro, tendo seu uso aplicado nas seguintes áreas:

 
- Segurança Pública;
- Internet das Coisas;
- Comércio;


### Referências Bibliográficas
	
1.	https://www.guru99.com/blockchain-tutorial.html
2.	https://medium.com/programmers-blockchain/create-simple-blockchain-java-tutorial-from-scratch-6eeed3cb03fa
3.	https://hackernoon.com/a-beginners-guide-to-blockchain-programming-4913d16eae31
